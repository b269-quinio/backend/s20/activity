// Part 1

let input = prompt("Please entire your desired number:");

console.log("The number you provided is " + input + ".");

for(let count = input; count <= 100; count--){

	if(count % 10 === 0 && count > 50) {
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	} else if (count % 5 === 0 && count > 50){
		console.log(count);
	} else if (count <= 50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}
}

// Part 2

let word = "supercalifragilisticexpialidocious";
let cons = 's';

console.log(word);

for(let index = 0; index < word.length; index++){

	if(
		word[index].toLowerCase() === "a" ||
		word[index].toLowerCase() === "e" ||
		word[index].toLowerCase() === "i" ||
		word[index].toLowerCase() === "o" ||
		word[index].toLowerCase() === "u" 
		){

	}else if(index === 2 || index === 22){ //p
		cons = cons + word[2];
	}else if(index === 4 || index === 10){ //r 
	  	cons = cons + word[4];
	}else if(index === 5 || index === 19 || index === 29){ //c
	  	cons = cons + word[5];
	}else if(index === 7 || index === 14 || index === 25){ //l
	  	cons = cons + word[7];
	}else if(index === 9){ //f
	  	cons = cons + word[9];
	}else if(index === 12){ //g
	  	cons = cons + word[12];
	}else if(index === 16){ //s
	  	cons = cons + word[16];
	}else if(index === 17){ //t
	  	cons = cons + word[17];
	}else if(index === 21){ //x
	  	cons = cons + word[21];
	}else if(index === 27){ //d
	  	cons = cons + word[27];
	}else if(index === 33){ //s
	  	cons = cons + word[33];
	  	console.log(cons);
	}
	
}